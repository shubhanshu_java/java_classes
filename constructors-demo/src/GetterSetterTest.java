
public class GetterSetterTest {

	public static void main(String[] args) {
		
		GetterSettersDemo myObject = new GetterSettersDemo();
		myObject.setName("Rani");
		myObject.setContact("909099000");
		String nameCopy = myObject.getName();
		String contactCopy = myObject.getContact();
//		System.out.println(" Name of buddy is : "+myObject.getName());
//		System.out.println(" Contact of buddy is : "+myObject.getContact());
		
		System.out.println(" Name of buddy is : "+nameCopy);
		
		System.out.println(" Contact of buddy is : "+contactCopy);
		
	}

}
