
public class ConstructorDemo {
	
	// instance variable
	String info;
	String info_two;
	
	public ConstructorDemo(String info ,String info_two ) {
		this.info = info;
		this.info_two = info_two;
	}
	
	public void printValues() {
		System.out.println("info is :"+info+" and info_two is : "+info_two);
		System.out.println("hello..");
	}
	
}
